#include <player.h>
#include <file_name_enum.h>
#include <video_wrapper.h>

player::player(video_wrapper *video) :
		video(video) {

	pos = playerPos::pos_boat;
	gold = 0;
	_is_dead = false;
	_video_rep_num = 0;
	//
	for (int i = 0; i < MAX_TENTACLES; ++i) {
		_is_ten_max[i] = false;
	}
	//
	init_position();
	//
	_rep_pics[0] = deep_see_files::ds_not_use;
	_rep_pics[1] = deep_see_files::ds_not_use;
}

void player::set_ten_max(unsigned int ten_num, bool is_max) {
	//
	if (ten_num < MAX_TENTACLES) {
		_is_ten_max[ten_num] = is_max;
	}
	if (is_max) {
		if (pos == ten_num + 1) {
			if (gold > 0)
				video->drawPicture(data[pos].pic_pl_gold_ten);
			else
				video->drawPicture(data[pos].pic_pl_ten);
		}
	}
	//
}

void player::move_left() {
	//
	if (pos == playerPos::pos_dead || pos == playerPos::pos_need_new)
		return;
	if (_video_rep_num > 0)
		return;
	//
	if (pos == playerPos::pos_boat) {
	} else if (pos == playerPos::pos_sh1) {
		video->drawPicture(data[pos].pic_empty);
		video->drawPicture(data[pos - 1].pic_pl);
		if (gold > 0) {
			_video_rep_num = 6;
			_rep_pics[0] = data[0].pic_pl_gold;
			_rep_pics[1] = data[0].pic_pl;
		}
		pos = static_cast<playerPos>(pos - 1);
	} else {
		video->drawPicture(data[pos].pic_empty);
		if (gold > 0) {
			if (_is_ten_max[pos - 2])
				video->drawPicture(data[pos - 1].pic_pl_gold_ten);
			else
				video->drawPicture(data[pos - 1].pic_pl_gold);
		} else {
			if (_is_ten_max[pos - 2])
				video->drawPicture(data[pos - 1].pic_pl_ten);
			else
				video->drawPicture(data[pos - 1].pic_pl);
		}
		pos = static_cast<playerPos>(pos - 1);
	}
}

void player::move_right() {
	//
	if (pos == playerPos::pos_dead || pos == playerPos::pos_need_new)
		return;
	if (_video_rep_num > 0)
		return;
	//
	if (pos == playerPos::pos_sh5) {
		++gold;
		video->drawPicture(data[pos].pic_pl_gold_take);
	} else {
		video->drawPicture(data[pos].pic_empty);
		if (gold > 0) {
			if (_is_ten_max[pos])
				video->drawPicture(data[pos + 1].pic_pl_gold_ten);
			else
				video->drawPicture(data[pos + 1].pic_pl_gold);
		} else {
			if (_is_ten_max[pos])
				video->drawPicture(data[pos + 1].pic_pl_ten);
			else
				video->drawPicture(data[pos + 1].pic_pl);
		}
		pos = static_cast<playerPos>(pos + 1);
	}
}

void player::player_dead() {
	//
	if (_is_ten_max[pos - 1])
		video->drawPicture(data[pos].pic_empty_ten);
	else
		video->drawPicture(data[pos].pic_empty);
	pos = playerPos::pos_dead;
	gold = 0;
	_is_dead = true;
	//
	_video_rep_num = 5;
	_rep_pics[0] = deep_see_files::ds_dead1;
	_rep_pics[1] = deep_see_files::ds_dead2;
	video->drawPicture(_rep_pics[_video_rep_num % 2]);
	//
}

void player::set_pos(playerPos new_pos) {
	//
	if (pos == playerPos::pos_dead || pos == playerPos::pos_need_new) {
	} else if (pos == playerPos::pos_boat) {
		video->drawPicture(data[0].pic_empty);
	} else {
		if (_is_ten_max[pos - 1]) {
			video->drawPicture(data[pos].pic_empty_ten);
		} else {
			video->drawPicture(data[pos].pic_empty);
		}
	}
	//
	pos = new_pos;
	//
	if (pos == playerPos::pos_dead || pos == playerPos::pos_need_new) {
	} else if (pos == playerPos::pos_boat) {
		video->drawPicture(data[0].pic_pl);
	} else {
		if (gold > 0) {
			if (_is_ten_max[pos - 1]) {
				video->drawPicture(data[pos].pic_pl_gold_ten);
			} else {
				video->drawPicture(data[pos].pic_pl_gold);
			}
		} else {
			if (_is_ten_max[pos - 1]) {
				video->drawPicture(data[pos].pic_pl_ten);
			} else {
				video->drawPicture(data[pos].pic_pl);
			}
		}
	}
	//
}

void player::tick() {
	//
	if (_video_rep_num > 0) {
		video->drawPicture(_rep_pics[_video_rep_num % 2]);
		--_video_rep_num;
	}
	if (_video_rep_num == 0) {
		if (pos == playerPos::pos_dead) {
			pos = playerPos::pos_need_new;
			video->drawPicture(deep_see_files::ds_dead_emp);
		}
	}
	//
	if (pos == playerPos::pos_sh5) {
		if (gold > 0) {
			video->drawPicture(data[pos].pic_pl_gold);
		} else {
			video->drawPicture(data[pos].pic_pl);
		}
	}
	//
}

void player::init_position() {
	//
	data[0].pic_empty = deep_see_files::ds_diver1_emp;
	data[0].pic_empty_ten = deep_see_files::ds_not_use;
	data[0].pic_pl = deep_see_files::ds_diver1_ng;
	data[0].pic_pl_gold = deep_see_files::ds_diver1_g;
	data[0].pic_pl_take = deep_see_files::ds_not_use;
	data[0].pic_pl_teke_ten = deep_see_files::ds_not_use;
	data[0].pic_pl_ten = deep_see_files::ds_not_use;
	data[0].pic_pl_gold_ten = deep_see_files::ds_not_use;
	data[0].pic_pl_gold_take = deep_see_files::ds_not_use;
	data[0].pic_pl_gold_take_ten = deep_see_files::ds_not_use;
	//
	data[1].pic_empty = deep_see_files::ds_diver1_p1_emp_g_emp_s1_4_emp;
	data[1].pic_empty_ten = deep_see_files::ds_diver1_p1_emp_g_emp_s1_4;
	data[1].pic_pl = deep_see_files::ds_diver1_p1_g_emp_s1_4_emp;
	data[1].pic_pl_gold = deep_see_files::ds_diver1_p1_g_s1_4_emp;
	data[1].pic_pl_ten = deep_see_files::ds_diver1_p1_g_emp_s1_4;
	data[1].pic_pl_gold_ten = deep_see_files::ds_diver1_p1_g_s1_4;
	data[1].pic_pl_take = deep_see_files::ds_not_use;
	data[1].pic_pl_teke_ten = deep_see_files::ds_not_use;
	data[1].pic_pl_gold_take = deep_see_files::ds_not_use;
	data[1].pic_pl_gold_take_ten = deep_see_files::ds_not_use;
	//
	data[2].pic_empty = deep_see_files::ds_diver1_p2_emp_g_emp_s2_4_emp;
	data[2].pic_empty_ten = deep_see_files::ds_diver1_p2_emp_g_emp_s2_4;
	data[2].pic_pl = deep_see_files::ds_diver1_p2_g_emp_s2_4_emp;
	data[2].pic_pl_gold = deep_see_files::ds_diver1_p2_g_s2_4_emp;
	data[2].pic_pl_ten = deep_see_files::ds_diver1_p2_g_emp_s2_4;
	data[2].pic_pl_gold_ten = deep_see_files::ds_diver1_p2_g_s2_4;
	data[2].pic_pl_take = deep_see_files::ds_not_use;
	data[2].pic_pl_teke_ten = deep_see_files::ds_not_use;
	data[2].pic_pl_gold_take = deep_see_files::ds_not_use;
	data[2].pic_pl_gold_take_ten = deep_see_files::ds_not_use;
	//
	data[3].pic_empty = deep_see_files::ds_diver1_p3_emp_g_emp_s3_5_emp;
	data[3].pic_empty_ten = deep_see_files::ds_diver1_p3_emp_g_emp_s3_5;
	data[3].pic_pl = deep_see_files::ds_diver1_p3_g_emp_s3_5_emp;
	data[3].pic_pl_gold = deep_see_files::ds_diver1_p3_g_s3_5_emp;
	data[3].pic_pl_ten = deep_see_files::ds_diver1_p3_g_emp_s3_5;
	data[3].pic_pl_gold_ten = deep_see_files::ds_diver1_p3_g_s3_5;
	data[3].pic_pl_take = deep_see_files::ds_not_use;
	data[3].pic_pl_teke_ten = deep_see_files::ds_not_use;
	data[3].pic_pl_gold_take = deep_see_files::ds_not_use;
	data[3].pic_pl_gold_take_ten = deep_see_files::ds_not_use;
	//
	data[4].pic_empty = deep_see_files::ds_diver1_p4_emp_g_emp_s4_4_emp;
	data[4].pic_empty_ten = deep_see_files::ds_diver1_p4_emp_g_emp_s4_4;
	data[4].pic_pl = deep_see_files::ds_diver1_p4_g_emp_s4_4_emp;
	data[4].pic_pl_gold = deep_see_files::ds_diver1_p4_g_s4_4_emp;
	data[4].pic_pl_ten = deep_see_files::ds_diver1_p4_g_emp_s4_4;
	data[4].pic_pl_gold_ten = deep_see_files::ds_diver1_p4_g_s4_4;
	data[4].pic_pl_take = deep_see_files::ds_not_use;
	data[4].pic_pl_teke_ten = deep_see_files::ds_not_use;
	data[4].pic_pl_gold_take = deep_see_files::ds_not_use;
	data[4].pic_pl_gold_take_ten = deep_see_files::ds_not_use;
	//
	data[5].pic_empty = deep_see_files::ds_diver1_p5_emp_g_emp_s5_3_emp_t_emp;
	data[5].pic_empty_ten = deep_see_files::ds_diver1_p5_emp_g_emp_s5_3_t_emp;
	data[5].pic_pl = deep_see_files::ds_diver1_p5_g_emp_s5_3_emp_t_emp;
	data[5].pic_pl_gold = deep_see_files::ds_diver1_p5_g_s5_3_emp_t_emp;
	data[5].pic_pl_ten = deep_see_files::ds_diver1_p5_g_emp_s5_3_t_emp;
	data[5].pic_pl_gold_ten = deep_see_files::ds_diver1_p5_g_s5_3_t_emp;
	data[5].pic_pl_take = deep_see_files::ds_diver1_p5_g_emp_s5_3_emp_t;
	data[5].pic_pl_teke_ten = deep_see_files::ds_diver1_p5_g_emp_s5_3_t;
	data[5].pic_pl_gold_take = deep_see_files::ds_diver1_p5_g_s5_3_emp_t;
	data[5].pic_pl_gold_take_ten = deep_see_files::ds_diver1_p5_g_s5_3_t;
	//
}

