#include <file_name_enum.h>
#include <main_logic.h>
#include <player.h>
#include <video_wrapper.h>
#include <cstdlib>

main_logic::main_logic(video_wrapper *video) :
		video(video), cur_player(video), tentacles { { 3, video }, { 4, video },
				{ 5, video }, { 4, video }, { 3, video } } {
	init_tentacles();
	//
	_game_stop = false;
	//
}

void main_logic::init() {
	//
	g_data = { };
	g_data.p_num = 3;
	g_data.scope = 0;
	//
	video->drawPicture(deep_see_files::ds_empty);
	//
	draw_players();
	//
	tentacles[0].setPos(0);
	tentacles[1].setPos(0);
	tentacles[2].setPos(0);
	tentacles[3].setPos(0);
	tentacles[4].setPos(0);
	//
	if (cur_player.curr_pos() == player::playerPos::pos_need_new) {
		init_new_player();
	}
	//
	show_scope();
	//
}

void main_logic::l_main(coreEvent event) {
	//
	if (event == coreEvent::timerTick) {
		tick();
	}
	if (event == coreEvent::btnUpLeft || event == coreEvent::btnDownLeft) {
		move_left();
	}
	if (event == coreEvent::btnUpRight || event == coreEvent::btnDownRight) {
		move_right();
	}
	//
}

void main_logic::tick() {
	//
	show_scope();
	cur_player.tick();
	//
	if (_game_stop)
		return;
	//
	if (cur_player.curr_pos() == player::playerPos::pos_dead)
		return;
	//
	auto ten_num = std::rand() % 5;
	if (ten_num == 0) {
		if (tentacles[1].pos() <= 1)
			tentacles[ten_num].move();
	} else if (ten_num == 1) {
		if (tentacles[0].pos() <= 1)
			tentacles[ten_num].move();
	} else {
		tentacles[ten_num].move();
	}
	//
	cur_player.set_ten_max(ten_num, tentacles[ten_num].isMax());
	//
	if (cur_player.curr_pos() == player::playerPos::pos_need_new) {
		init_new_player();
	}
	//
	check_p_pos();
	//
}

void main_logic::move_left() {
	//
	if (_game_stop)
		return;
	//
	auto pl_pos = cur_player.curr_pos();
	if (pl_pos != player::playerPos::pos_dead) {
		//
		cur_player.move_left();
		check_p_pos();
		//
	}
	//
	if (pl_pos == player::playerPos::pos_sh1) {
		g_data.scope += cur_player.take_gold();
		show_scope();
	}
	//
}

void main_logic::move_right() {
	//
	if (_game_stop)
		return;
	//
	auto pl_pos = cur_player.curr_pos();
	if (pl_pos != player::playerPos::pos_dead) {
		//
		cur_player.move_right();
		check_p_pos();
		//
	}
	//
}

void main_logic::check_p_pos() {
	//
	unsigned int pl_dead = cur_player.curr_pos() == player::playerPos::pos_sh1
			&& tentacles[0].isMax();
	pl_dead += cur_player.curr_pos() == player::playerPos::pos_sh2
			&& tentacles[1].isMax();
	pl_dead += cur_player.curr_pos() == player::playerPos::pos_sh3
			&& tentacles[2].isMax();
	pl_dead += cur_player.curr_pos() == player::playerPos::pos_sh4
			&& tentacles[3].isMax();
	pl_dead += cur_player.curr_pos() == player::playerPos::pos_sh5
			&& tentacles[4].isMax();
	//
	if (pl_dead > 0) {
		tentacles[2].setPos(2);
		tentacles[3].setPos(2);
		cur_player.set_ten_max(2, false);
		cur_player.set_ten_max(3, false);
		cur_player.player_dead();
	}
	//
}

void main_logic::init_new_player() {
	//
	tentacles[2].setPos(0);
	tentacles[3].setPos(0);
	//
	g_data.p_num--;
	if (g_data.p_num <= 0)
		_game_stop = true;
	else {
		cur_player.set_pos(player::playerPos::pos_boat);
		draw_players();
	}
	//
}

void main_logic::draw_players() {
	//
	if (g_data.p_num < 3) {
		video->drawPicture(deep_see_files::ds_diver3_emp);
	} else {
		video->drawPicture(deep_see_files::ds_diver3);
	}
	//
	if (g_data.p_num < 2) {
		video->drawPicture(deep_see_files::ds_diver2_emp);
	} else {
		video->drawPicture(deep_see_files::ds_diver2);
	}
	//
	if (g_data.p_num < 1) {
		video->drawPicture(deep_see_files::ds_diver1_emp);
	} else {
		video->drawPicture(deep_see_files::ds_diver1_ng);
	}
	//
}

void main_logic::show_scope() {
	//
	auto curr_scope = g_data.scope;
	//
	const uint16_t X_OFFSET = 100;
	const uint16_t Y_OFFSET = 62;
	const uint16_t Y_DIG_OFFSET = 12;
	const uint16_t MAX_DIG = 6;
	uint8_t pos = MAX_DIG;
	while (pos > 0) {
		//
		uint32_t dig_vis;
		switch (curr_scope % 10) {
		case 0:
			if (curr_scope > 0 || pos == MAX_DIG) {
				dig_vis = deep_see_files::ds_num0;
			} else {
				dig_vis = deep_see_files::ds_num_emp;
			}
			break;
		case 1:
			dig_vis = deep_see_files::ds_num1;
			break;
		case 2:
			dig_vis = deep_see_files::ds_num2;
			break;
		case 3:
			dig_vis = deep_see_files::ds_num3;
			break;
		case 4:
			dig_vis = deep_see_files::ds_num4;
			break;
		case 5:
			dig_vis = deep_see_files::ds_num5;
			break;
		case 6:
			dig_vis = deep_see_files::ds_num6;
			break;
		case 7:
			dig_vis = deep_see_files::ds_num7;
			break;
		case 8:
			dig_vis = deep_see_files::ds_num8;
			break;
		case 9:
			dig_vis = deep_see_files::ds_num9;
			break;
		default:
			break;
		}
		video->drawPicture(X_OFFSET, static_cast<uint16_t>(Y_OFFSET + Y_DIG_OFFSET * pos), dig_vis);
		curr_scope = curr_scope / 10;
		pos--;
		//
	}
	//
}

void main_logic::init_tentacles() {
	//
	tentacles[0].addSegment(0, deep_see_files::ds_s1_1_emp,
			deep_see_files::ds_s1_1_emp);
	tentacles[0].addSegment(1, deep_see_files::ds_s1_1,
			deep_see_files::ds_s1_1_emp);
	tentacles[0].addSegment(2, deep_see_files::ds_s1_2,
			deep_see_files::ds_s1_2_emp);
	tentacles[0].addSegment(3, deep_see_files::ds_s1_3,
			deep_see_files::ds_s1_3_emp);
	//
	tentacles[1].addSegment(0, deep_see_files::ds_s2_1_emp,
			deep_see_files::ds_s2_1_emp);
	tentacles[1].addSegment(1, deep_see_files::ds_s2_1,
			deep_see_files::ds_s2_1_emp);
	tentacles[1].addSegment(2, deep_see_files::ds_s2_2,
			deep_see_files::ds_s2_2_emp);
	tentacles[1].addSegment(3, deep_see_files::ds_s2_3,
			deep_see_files::ds_s2_3_emp);
	tentacles[1].addSegment(4, deep_see_files::ds_s2_4,
			deep_see_files::ds_s2_4_emp);
	//
	tentacles[2].addSegment(0, deep_see_files::ds_s3_1_emp,
			deep_see_files::ds_s3_1_emp);
	tentacles[2].addSegment(1, deep_see_files::ds_s3_1,
			deep_see_files::ds_s3_1_emp);
	tentacles[2].addSegment(2, deep_see_files::ds_s3_2,
			deep_see_files::ds_s3_2_emp);
	tentacles[2].addSegment(3, deep_see_files::ds_s3_3,
			deep_see_files::ds_s3_3_emp);
	tentacles[2].addSegment(4, deep_see_files::ds_s3_4,
			deep_see_files::ds_s3_4_emp);
	tentacles[2].addSegment(5, deep_see_files::ds_s3_5,
			deep_see_files::ds_s3_5_emp);
	//
	tentacles[3].addSegment(0, deep_see_files::ds_s4_1_emp,
			deep_see_files::ds_s4_1_emp);
	tentacles[3].addSegment(1, deep_see_files::ds_s4_1,
			deep_see_files::ds_s4_1_emp);
	tentacles[3].addSegment(2, deep_see_files::ds_s4_2,
			deep_see_files::ds_s4_2_emp);
	tentacles[3].addSegment(3, deep_see_files::ds_s4_3,
			deep_see_files::ds_s4_3_emp);
	tentacles[3].addSegment(4, deep_see_files::ds_s4_4,
			deep_see_files::ds_s4_4_emp);
	//
	tentacles[4].addSegment(0, deep_see_files::ds_s5_1_emp,
			deep_see_files::ds_s5_1_emp);
	tentacles[4].addSegment(1, deep_see_files::ds_s5_1,
			deep_see_files::ds_s5_1_emp);
	tentacles[4].addSegment(2, deep_see_files::ds_s5_2,
			deep_see_files::ds_s5_2_emp);
	tentacles[4].addSegment(3, deep_see_files::ds_s5_3,
			deep_see_files::ds_s5_3_emp);
	//
}

