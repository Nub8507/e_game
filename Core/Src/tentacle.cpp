#include <tentacle.h>
#include <video_wrapper.h>

tentacle::tentacle(uint8_t size, video_wrapper *video) :
		video(video), ten_size(size) {
	curr_pos = 0;
	dir = direction::Inc;
}

void tentacle::addSegment(uint8_t pos, deep_see_files pic_in,
		deep_see_files pic_out) {
	//
	if (pos > ten_size)
		return;
	//
	data[pos].pic_in = pic_in;
	data[pos].pic_out = pic_out;
	//
}

void tentacle::setPos(uint8_t pos) {
	if (pos > ten_size)
		return;
	//
	while (pos != curr_pos) {
		if (pos > curr_pos) {
			inc();
		} else {
			dec();
		}
	}
	//
	if (this->isMax())
		dir = direction::Dec;
	else
		dir = direction::Inc;
	//
}

void tentacle::inc() {
	//
	if (curr_pos < ten_size) {
		curr_pos++;
		video->drawPicture(data[curr_pos].pic_in);
	}
	//
}

void tentacle::dec() {
	if (curr_pos > 0) {
		video->drawPicture(data[curr_pos].pic_out);
		curr_pos--;
		video->drawPicture(data[curr_pos].pic_in);
	}

}

void tentacle::move() {
	//
	if (isMax()) {
		dir = direction::Dec;
	} else if (isMin()) {
		dir = direction::Inc;
	}
	//
	if (dir == direction::Inc) {
		inc();
	} else {
		dec();
	}
	//
}

bool tentacle::isMax() const {
	return curr_pos == ten_size;
}

bool tentacle::isMin() const {
	return curr_pos == 0;
}

uint8_t tentacle::pos() const {
	return curr_pos;
}

