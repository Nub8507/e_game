#include <sd_sdio.h>
#include <st7735.h>
#include <video_wrapper.h>
#include <cstring>

video_wrapper::video_wrapper(interrupts &my_inter, st7735 *video,
		sd_sdio *sd_card) :
		X_SIZE(128), Y_SIZE(160), my_interrupt(my_inter), video(video), sd_card(
				sd_card) {
	status = video_status::NOT_INIT;
}

void video_wrapper::drawPicture(int32_t id) {

	if (id < 0)
		return;
	pic_data rez;
	if (id < header_num && images_header[id].data_id == id) {
		rez = images_header[id];
	}
	draw_buff.push_back(rez);
}

void video_wrapper::drawPicture(uint16_t pos_x, uint16_t pos_y, int32_t id) {

	if (id < 0)
		return;
	pic_data rez = { };
	if (id < header_num && images_header[id].data_id == id) {
		rez = images_header[id];
		rez.pos_x = pos_x;
		rez.pos_y = pos_y;
	}
	draw_buff.push_back(rez);
}

bool video_wrapper::readImagesHeader() {
	//
	uint32_t header_size;
	sd_card->read_data(*screen_buffer, 0, 4);
	memcpy(&header_size, *screen_buffer, 4);
	memcpy(&header_num, *screen_buffer + 4, 2);
	if (header_num >= MAX_HEADER_SIZE) {
		return false;
	}
	sd_card->read_data(*screen_buffer, 0, header_size + 6);
	//
	for (auto i = 0; i < header_num; ++i) {
		pic_data data;
		memcpy(&data, *screen_buffer + 6 + sizeof(pic_data) * i,
				sizeof(pic_data));
		images_header[i] = data;
	}
	return true;
	//
}

void video_wrapper::init() {
	if (video == nullptr) {
		status = video_status::NOT_INIT;
		return;
	}
	if (sd_card == nullptr) {
		status = video_status::NOT_INIT;
		return;
	}
	screen_buffer = video->get_buf_ptr();
	if (screen_buffer == nullptr) {
		status = video_status::NOT_INIT;
		return;
	}
	if (!readImagesHeader()) {
		status = video_status::NOT_INIT;
		return;
	}
}

void video_wrapper::tick() {
	//
	if (draw_buff.size() > 0) {
		//
		auto last_pic = draw_buff.pop_first();
		if (last_pic.data_id == -1)
			return;
		auto size = images_header[last_pic.data_id].data_size;
		auto addres = images_header[last_pic.data_id].addres;
		auto val = sd_card->read_data(*screen_buffer, addres, size);
		if (val == false) {
			return;
		}
		//
		st7735::st7735_size p_size; //( ,
		p_size.w = static_cast<uint16_t>(images_header[last_pic.data_id].width);
		p_size.h =
				static_cast<uint16_t>(images_header[last_pic.data_id].height);
		st7735::st7735_pos pos = { last_pic.pos_x, last_pic.pos_y };
		//auto val=*screen_buffer;
		video->draw_image(pos, p_size); //, reinterpret_cast<uint16_t *>(val));
	}
}
