#ifndef MAIN_LOGIC_H_
#define MAIN_LOGIC_H_

#include <main_defs.h>
#include <player.h>
#include <sys/_stdint.h>
#include <tentacle.h>

struct interrupts;
class video_wrapper;

class main_logic {
public:

	main_logic(video_wrapper *video);
	virtual ~main_logic() {
	}

	void init();
	void l_main(coreEvent event);

private:

	struct g_state {
		//
		unsigned int scope;
		uint8_t p_num;
		//

		g_state() {
			scope = 0;
			p_num = 3;
		}

	};

	video_wrapper *const video;
	g_state g_data;
	bool _game_stop;

	player cur_player;
	tentacle tentacles[MAX_TENTACLES];

	void init_tentacles();
	void draw_players();
	void tick();
	void move_left();
	void move_right();
	void check_p_pos();
	void init_new_player();
	void show_scope();

};

#endif /* MAIN_LOGIC_H_ */
