#ifndef INC_PLAYER_H_
#define INC_PLAYER_H_

#include <sys/_stdint.h>
#include "main_defs.h"
#include "file_name_enum.h"

class video_wrapper;

using _uint = unsigned int;

class player {
public:

	enum playerPos {
		pos_boat					= 0,
		pos_sh1,
		pos_sh2,
		pos_sh3,
		pos_sh4,
		pos_sh5,
		pos_dead,
		pos_need_new,
	};

	struct position {
		deep_see_files pic_empty;
		deep_see_files pic_empty_ten;
		deep_see_files pic_pl_ten;
		deep_see_files pic_pl_gold_ten;
		deep_see_files pic_pl;
		deep_see_files pic_pl_gold;
		deep_see_files pic_pl_take;
		deep_see_files pic_pl_teke_ten;
		deep_see_files pic_pl_gold_take;
		deep_see_files pic_pl_gold_take_ten;
	};


	player(video_wrapper *video = nullptr);
	virtual ~player() = default;
	player(const player &other) = delete;
	player(player &&other) = delete;
	player& operator=(const player &other) = delete;
	player& operator=(player &&other) = delete;

	void tick();

	playerPos curr_pos() const { return pos; }
	unsigned int take_gold() { auto t = gold; gold = 0; return t; }
	void set_pos(playerPos new_pos);
	void player_dead();
	void move_left();
	void move_right();
	void set_ten_max(unsigned int ten_num, bool is_max);

private:

	video_wrapper *video;


	playerPos pos;
	uint32_t gold;
	bool _is_dead;
	_uint _video_rep_num;

	bool _is_ten_max[MAX_TENTACLES];
	position data[MAX_PLAYER_POS];

	deep_see_files _rep_pics[2];

	void init_position();

};

#endif /* INC_PLAYER_H_ */
