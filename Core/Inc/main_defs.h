#ifndef MAIN_DEFS_H_
#define MAIN_DEFS_H_

#include <sys/_stdint.h>

#ifdef __cplusplus

enum class coreEvent {
	picRead,
	picShow,
	timerTick,
	btnUpLeft,
	btnDownLeft,
	btnUpRight,
	btnDownRight,
};

struct pic_data {
	int32_t data_id;
	uint32_t data_size;
	uint32_t width;
	uint32_t height;
	uint16_t pos_x;
	uint16_t pos_y;
	uint32_t addres;

	pic_data() {
		data_id = -1;
	}
};

#endif

static const uint8_t MAX_TENTACLES = 5;
static const uint8_t MAX_PLAYER_POS = 6;

struct interrupts {
	//
	uint8_t btn_r_up;
	uint8_t btn_r_down;
	uint8_t btn_l_up;
	uint8_t btn_l_down;
	uint8_t spi1_half;
	uint8_t spi1_full;
	uint8_t st7735_ready;
	uint8_t sdio_half;
	uint8_t sdio_full;
	uint8_t sdio_ready;
	uint8_t tick_timer;
	//
};

#endif /* MAIN_DEFS_H_ */
