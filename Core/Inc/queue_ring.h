#ifndef QUEUE_RING_H
#define QUEUE_RING_H

#include <cstddef>

template<typename T,size_t buf_size>
class queue_ring
{
public:
	queue_ring();

	template<typename T2>
	queue_ring(T2 &&def_val);

	size_t size();
    void push_back(const T &val);
    T pop_first();

private:
    T buffer[buf_size];
    size_t in,out;

};

template<typename T, size_t buf_size>
queue_ring<T,buf_size>::queue_ring()
{
	in = 0;
	out = 0;
}

template<typename T, size_t buf_size>
template<typename T2>
queue_ring<T,buf_size>::queue_ring(T2 &&def_val) {
	for (size_t i = 0; i < buf_size; ++i) {
		buffer[i] = def_val;
	}
	in = 0;
	out = 0;
}


template<typename T, size_t buf_size>
size_t queue_ring<T,buf_size>::size()
{
    //
    if(in>=out){
        return in-out;
    }else{
        return buf_size-out+in;
    }
    //
}

template<typename T, size_t buf_size>
void queue_ring<T,buf_size>::push_back(const T &val)
{
    in++;
    if(in>=buf_size)in=0;
    buffer[in]=val;
}

template<typename T, size_t buf_size>
T queue_ring<T,buf_size>::pop_first()
{
    if(out==in) return {};
    out++;
    if(out>=buf_size)out=0;
    return buffer[out];
}

#endif // QUEUE_RING_H
