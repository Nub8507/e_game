#ifndef TENTACLE_H
#define TENTACLE_H

#include <file_name_enum.h>
#include <sys/_stdint.h>

class video_wrapper;
class tentacle {
public:

	tentacle(uint8_t size, video_wrapper *video = nullptr);

	void addSegment(uint8_t pos, deep_see_files pic_in, deep_see_files pic_out);
	void setPos(uint8_t pos);
	void inc();
	void dec();
	void move();

	bool isMax() const;
	bool isMin() const;
	uint8_t pos() const;

private:

	enum class direction {
		Inc, Dec,
	};

	struct segment {
		deep_see_files pic_in;
		deep_see_files pic_out;
	};

	static const uint8_t MAX_SIZE = 6;

	video_wrapper *video;
	uint8_t ten_size;
	uint8_t curr_pos;
	segment data[MAX_SIZE];
	direction dir;
};

#endif // TENTACLE_H
