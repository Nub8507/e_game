#ifndef VIDEOWRAPPER_H_
#define VIDEOWRAPPER_H_

#include <main_defs.h>
#include <queue_ring.h>
#include <sys/_stdint.h>

struct interrupts;
class st7735;
class sd_sdio;

class video_wrapper {
public:
	video_wrapper(interrupts &my_inter, st7735 *video, sd_sdio *sd_card);
	virtual ~video_wrapper() {
	}
	video_wrapper(video_wrapper &&other) = delete;
	video_wrapper(const video_wrapper &other) = delete;
	video_wrapper& operator=(const video_wrapper &other) = delete;
	video_wrapper& operator=(video_wrapper &&other) = delete;

	enum class video_status {
		READY,
		BUSY,
		QUEUE_FULL,
		NOT_INIT,

	};

	void drawPicture(int32_t id);
	void drawPicture(uint16_t pos_x, uint16_t pos_y, int32_t id);

	void init();
	void tick();

	video_status getStatus() const {
		return status;
	}

private:

	const uint8_t X_SIZE;
	const uint8_t Y_SIZE;
	static const uint8_t MAX_HEADER_SIZE = 150;

	interrupts &my_interrupt;
	st7735 *video;
	sd_sdio *sd_card;

	uint8_t **screen_buffer;
	pic_data images_header[MAX_HEADER_SIZE];
	video_status status;

	uint16_t header_num;

	queue_ring<pic_data, 50> draw_buff;

	bool readImagesHeader();

};

#endif /* VIDEOWRAPPER_H_ */
