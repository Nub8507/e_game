#include "create_image.h"

#include <QDir>
#include <QFile>
#include <QString>
#include <QImage>
#include <QtEndian>
#include <algorithm>
#include <QDebug>


/*
 * структура файла данных для записи на sd
 *
 * uint32_t header_size - размер заголовка
 * uint16_t rec_num - количество записей в залоловке
 * e_games::pic_data * rec_num - заголовочные запси для данных
 * -----------выравнивание по 512 байт
 * QByteArray * rec_num - блоки данных, размер и адреса начала блоков
 *                        находятся в заголовке
 * -----------каждый блок данных выровнен по 512 байт
*/

QPair<QByteArray,QStringList> create_image::operator()(QStringList file_list,quint32 offset)
{
    //
    rez_list.clear();
    //
    QByteArray ba_file;
    //
    QStringList enum_list;
    QPair<QByteArray,QStringList> rez;
    //
    uint16_t fileNum=0;
    for(const auto &file:file_list){
        qDebug()<<"file = "<<file;
        QImage imj=QImage(file);
        if(imj.isNull()){
            continue;
        }
        //imj=imj.convertToFormat(QImage::Format_RGB16);
        auto bits=imj.bits();
        auto size=imj.width()*imj.height() * 2;
        ba_file = convert_data_rgb888_to_rgb565(imj);
        e_games::pic_data data;
        data.width=static_cast<quint32>(imj.width());
        data.height=static_cast<quint32>(imj.height());
        QString f_name="ds_"+QFileInfo(file).completeBaseName().toLower();
        QString f_pos=f_name.right(7);
        f_name=f_name.left(f_name.size()-7);
        enum_list.append(f_name);
        auto tt1=f_pos.left(3);
        auto tt2=tt1.toInt();
        data.pos_x=static_cast<uint16_t>(f_pos.left(3).toInt());
        data.pos_y=static_cast<uint16_t>(f_pos.right(3).toInt());
        data.data_id=fileNum;
        data.data_size=static_cast<quint32>(size);
        data.addres=0;
        rez_list.push_back({data,ba_file});
        fileNum++;
    }
    //
    rez.first=create_file(offset);
    rez.second=enum_list;
    //
    return rez;
    //
}

QPair<QByteArray,QStringList> create_image::operator()(QString dir_path,quint32 offset)
{
    //
    QDir dir;
    if(dir_path.isEmpty()){
        dir_path=QDir::currentPath()+"/bmp";
    }
    dir=QDir(dir_path);
    auto file_list=dir.entryList(QDir::Files,QDir::NoSort);
    int i=file_list.size()-1;
    while(i>=0){
        auto &s=file_list[i];
        if(s.right(4)!=".bmp"){
            file_list.removeAt(i);
        }else{
           s=dir_path+"/"+s;
        }
        i--;
    }
    return operator()(file_list,offset);
    //
}

QByteArray create_image::create_file(quint32 offset)
{
    //
    offset+=4;  //добавим место для размера заголовка
    offset+=2;  //добавим место для количества записей
    QByteArray rez;
    QDataStream d_stream(&rez,QIODevice::WriteOnly);
    //создаем заголовок файла данных
    auto header=create_header_part(offset);
    d_stream.writeRawData(header.data(),header.size());
    //создаем массив данных
    auto data=create_data_part();
    d_stream.writeRawData(data.data(),data.size());
    //
    return rez;
    //
}

QByteArray create_image::create_header_part(quint32 offset)
{
    //
    QByteArray tmp;
    //вычисляем адрес начала блока данных с учетом начального смещения
    quint32 header_size=static_cast<quint32>(rez_list.size())*sizeof(e_games::pic_data);
    quint32 header_offset=header_size;
    header_offset+=offset;
    //добавим выравнивание на 512 байт для заголовка
    quint32 f=header_offset/512;
    quint32 s=header_offset%512;
    if(s!=0){
        header_offset=f*512+512;
    }
    //
    quint32 previos_offset=0;//смещение следующего блока данных относительно начала блока
    QDataStream d_stream(&tmp,QIODevice::WriteOnly);
    //выводим размер заголовка
    d_stream<<static_cast<uint32_t>(qToBigEndian(header_size));
    //выводим количество записей
    quint16  list_size=rez_list.size();
    d_stream<<static_cast<uint16_t>(qToBigEndian(list_size));
    for(auto &d:rez_list){
        //вычисляем адрес начала данных
        d.first.addres+=header_offset+previos_offset;
        //выводим описание очередного блока
        d_stream<<d.first;
        //получаем адрес следующего блока
        previos_offset=padding_to_512(previos_offset,d.first.data_size);
    }
    //заполмяем нулями остаток заголовка, полученный после выравнивания
    QByteArray header(static_cast<int>(header_offset),0);
    memcpy(header.data(),tmp.data(),static_cast<size_t>(tmp.size()));
    return header;
    //
}

QByteArray create_image::create_data_part() const
{
    //
    QByteArray tmp;
    QDataStream d_stream(&tmp,QIODevice::WriteOnly);
    for(auto &d:rez_list){
        auto data=padding_to_512(d.second);
        for(auto i=0;i<data.size()/2;++i)
            std::swap(data[2*i],data[2*i+1]);
        d_stream.writeRawData(data.data(),data.size());
    }
    return tmp;
    //
}

quint32 create_image::padding_to_512(quint32 last_pos, quint32 size)
{
    //
    quint32 f=size/512;
    quint32 s=size%512;
    if(s!=0){
        auto debug=f*512+512;
        return last_pos+f*512+512;
    }
    return last_pos+size;
    //
}

QByteArray create_image::padding_to_512(const QByteArray &data) const
{
    //
    quint32 d_size= static_cast<quint32>(data.size());
    quint32 f=d_size/512;
    quint32 s=d_size%512;
    if(s==0){
        return data;
    }
    QByteArray n_data(static_cast<int>(f*512+512),0);
    memcpy(n_data.data(),data.data(),static_cast<size_t>(data.size()));
    return n_data;
    //
}

uint16_t create_image::convert_rgb888_to_rgb565(uint16_t r, uint16_t g, uint16_t b)
{
    //
    uint16_t rez=0;
    rez|=(r & 0xF8)<<8;
    rez|=(g & 0xFC)<<3;
    rez|=(b & 0xF8)>>3;
    //
    return rez;
    //
}

QByteArray create_image::convert_data_rgb888_to_rgb565(const QImage &imj)
{
    //
    QByteArray data;
    QDataStream d_stream(&data,QIODevice::WriteOnly);
    //
    for(auto y=0;y<imj.height();y++){
        for(auto x=0;x<imj.width();x++){
            auto pix=imj.pixel(x,y);
            uint16_t r=qRed(pix);
            uint16_t g=qGreen(pix);
            uint16_t b=qBlue(pix);
            uint16_t rgb565=qToBigEndian(convert_rgb888_to_rgb565(r,g,b));
            d_stream<<rgb565;
        }
    }
    //
    return data;
}

QDataStream &operator<<(QDataStream &ds, e_games::pic_data data)
{
    //
    QByteArray ba(24,0);
    quint32 t32=0;
    quint16 t16=0;
    t32=qToLittleEndian(data.data_id);
    memcpy(ba.data(),&t32,4);
    t32=qToLittleEndian(data.data_size);
    memcpy(ba.data()+4,&t32,4);
    t32=qToLittleEndian(data.width);
    memcpy(ba.data()+8,&t32,4);
    t32=qToLittleEndian(data.height);
    memcpy(ba.data()+12,&t32,4);
    t16=qToLittleEndian(data.pos_x);
    memcpy(ba.data()+16,&t16,2);
    t16=qToLittleEndian(data.pos_y);
    memcpy(ba.data()+18,&t16,2);
    t32=qToLittleEndian(data.addres);
    memcpy(ba.data()+20,&t32,4);
    //
    ds.writeRawData(ba.data(),24);
    //
    return ds;
    //
}
