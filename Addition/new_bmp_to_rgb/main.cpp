#include <QCoreApplication>
#include <QFile>
#include <QDebug>
#include "create_image.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    create_image imj_creator;

    for(int i=1;i<argc;++i){
        QString s_arg=QString(argv[i]);
        auto arr=imj_creator(s_arg,0);
    }
    auto arr_pair=imj_creator("",0);
    QFile file("out_file.bin");
    if (file.open(QIODevice::WriteOnly)){
        QDataStream out(&file);
        out.writeRawData(arr_pair.first.data(),arr_pair.first.size());
        file.close();
    }
    QFile file_enum("out_file.enum");
    bool first = true;
    if (file_enum.open(QIODevice::WriteOnly)){
        QTextStream out(&file_enum);
        out<<"enum deep_see_files{\n";
        for(const auto &s:arr_pair.second){
            if(first){
                out<<"\t\t"<<s<<"\t\t\t\t\t\t\t\t= 0,\n";
                first = false;
            }else{
                out<<"\t\t"<<s<<",\n";
            }
        }
        out<<"\t\tds_not_use\t\t\t\t\t\t\t\t= 0xffff,\n";
        out<<"};\n";
        file_enum.close();
    }

    //
    //
    return 0;
}
