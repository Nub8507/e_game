#ifndef FILE_NAME_ENUM_H
#define FILE_NAME_ENUM_H

#ifdef QT_COMPILE
#include <QObject>
#endif

namespace e_games{

#ifdef QT_COMPILE
        Q_NAMESPACE
#endif

    enum deep_see_files{
        ds_num0                             =   1,
        ds_num1,
        ds_num2,
        ds_num3,
        ds_num4,
        ds_num5,
        ds_num6,
        ds_num7,
        ds_num8,
        ds_num9,
        ds_empty,
        ds_diver1_ng,
        ds_diver1_p1_emp_g_emp_s1_4_emp,
        ds_diver1_p1_g_emp_s1_4_emp,
        ds_diver1_p1_g_s1_4_emp,
        ds_diver1_p2_emp_g_emp_s2_4_emp,
        ds_diver1_p2_g_emp_s2_4_emp,
        ds_diver1_p2_g_s2_4_emp,
        ds_diver1_p3_emp_g_emp_s3_5_emp,
        ds_diver1_p3_g_emp_s3_5_emp,
        ds_diver1_p3_g_s3_5_emp,
        ds_diver1_p4_emp_g_emp_s4_4_emp,
        ds_diver1_p4_g_emp_s4_4_emp,
        ds_diver1_p4_g_s4_4_emp,
        ds_diver1_p5_emp_g_emp_s5_3_emp_t_emp,
        ds_diver1_p5_g_emp_s5_3_emp_2_t_emp,
        ds_diver1_p5_g_s5_3_emp_2_emp_t,
        ds_diver1_p5_g_s5_3_emp_2_t_emp,

    };

    struct pic_data{
        int32_t data_id;
        uint32_t data_size;
        uint32_t width;
        uint32_t height;
        uint16_t pos_x;
        uint16_t pos_y;
        uint32_t addres;
    };

#ifdef QT_COMPILE
    Q_ENUM_NS(deep_see_files)
#endif

}

#endif // FILE_NAME_ENUM_H
