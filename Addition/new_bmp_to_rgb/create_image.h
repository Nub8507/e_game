#ifndef CREATE_IMAGE_H
#define CREATE_IMAGE_H
#include <QMetaEnum>
#include <QObject>
#include <QList>
#include <QByteArray>
#include <QDataStream>

#include "file_name_enum.h"

class QString;

class create_image: public QObject
{
    Q_OBJECT
public:
    create_image(QObject* parent = nullptr):QObject(parent) {
       metaEnum = QMetaEnum::fromType<e_games::deep_see_files>();
    }
    QPair<QByteArray,QStringList> operator()(QStringList file_list, quint32 offset);
    QPair<QByteArray,QStringList> operator()(QString dir_path, quint32 offset);


private:

    QMetaEnum metaEnum;
    QList<QPair<e_games::pic_data,QByteArray>> rez_list;

    QByteArray create_file(quint32 offset);
    QByteArray create_header_part(quint32 offset);
    QByteArray create_data_part()const;
    quint32 padding_to_512(quint32 last_pos,quint32 size);
    QByteArray padding_to_512(const QByteArray &data)const;
    uint16_t convert_rgb888_to_rgb565(uint16_t r,uint16_t g, uint16_t b);
    QByteArray convert_data_rgb888_to_rgb565(const QImage &imj);


};

QDataStream& operator<<(QDataStream& ds,e_games::pic_data data);

#endif // CREATE_IMAGE_H
